import React, {useContext} from 'react'

/*************************React-Packages*******************************/
import DataTable from 'react-data-table-component';
import { 
    Link
  } from "react-router-dom";

import { apiToastError, apiToastSuccess } from "../../util/util";


/*************************Material-UI-Components*******************************/
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

/********************************Material Ui Icons******************************/
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';

/**********************************Global State********************************/
import { GlobalContext } from '../../Context/GlobalState';

function Home() {


    /********************************Globally accessed data*********************************/
        const {users , removeUser} = useContext(GlobalContext);
        console.log("users",users);

        const deleteUser = (id) =>{
            apiToastSuccess("User deleted successfully")
            removeUser(id)
        }
        
    /********************************Data of datatable*********************************/
        const data = users;

             

    /********************************Columns of datatable*********************************/
        const columns = [
        {
            name: 'First Name',
            selector: 'fname',
            sortable: true,
        },
        {
            name: 'Last Name',
            selector: 'lname',
            sortable: true,
    
        },
        {
            name: "Actions",
            cell: row => <div data-tag="allowRowEvents row">
                            <button className="btn btn-sm btn-primary mx-2 ">
                                <Link to={`/edit/${row.id}`}>
                                   <CreateIcon className="edit-button"></CreateIcon>
                                </Link>
                            </button>
                            <button className="btn btn-sm btn-danger" 
                            //  onClick={() => removeUser(row?.id)}
                                onClick={() => deleteUser(row.id)}
                            >
                                <DeleteIcon></DeleteIcon>
                            </button>
                        </div>,
        },
        ];
    return (
        <div className="container mt-4">
          {/* DataTable */}
             <DataTable
                title="Users"
                actions={<Link to="/add" className="m-3"><Fab size="small" color="secondary" aria-label="add"><AddIcon /></Fab></Link>}
                columns={columns}
                data={data}
                pagination
           />
        </div>
    )
}

export default Home   
