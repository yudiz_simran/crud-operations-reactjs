import React, {useContext, useEffect, useState} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { Container } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { apiToastError, apiToastSuccess } from "../../util/util";

/********************************React-Packages*****************************/
import {
    Link,
    useHistory
  } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { useFormik } from 'formik';
import { v4 as uuid} from 'uuid'

/**********************Yup-Validation****************************************/
import * as Yup from 'yup';

/**********************************Global State********************************/
import { GlobalContext } from '../../Context/GlobalState';
 

/**********************Validation-Schema****************************************/
const ValidationSchema = Yup.object().shape({
  fname: Yup.string()
    .required('First name is required')
    .matches(/^(\S)/, 'This field cannot contain only blankspaces'),
  lname: Yup.string()
    .required('Last name is required')
    .matches(/^(\S)/, 'This field cannot contain only blankspaces'),
});



export default function AddUser(props) {

 /********************************Globally accessed data*********************************/
 const {users,addUser,editUser} = useContext(GlobalContext);

 const [selectedUser, setUser] = useState({
     id:'',
     fname:'',
     lname:''
 })

 const history = useHistory(); 

    useEffect(() => {
        // const id = getId()
        // console.log("iddd",id);
        // const updatedUser = users.find(user => user.id === Number(id))
        
    });

/********************************get ID from url*********************************/
   const getId = () => {
    const { match } = props;
    
    return match?.params?.id;
    };

/********************************set initial values of form*********************************/
    function initialValues(){
        if(!getId()){
           return {
               fname:'',
               lname:''
           }
        }else{
            const id = getId()
            console.log("users-----",users);
            const updatedUser = users?.find(user => user.id === id)
            console.log("$$$",updatedUser)
            // setUser(updatedUser)
            return{
              fname:updatedUser?.fname,
              lname:updatedUser?.lname
            }
        }
    }

/********************************Form Submit Function*********************************/
    function onHandleSubmit(val){
      console.log("Val",val)
        const apiVal = {
            id:uuid(),
            fname: val.fname,
            lname: val.lname,
        };
       
        const editapiVal = {
            id:getId(),
            fname: val.fname,
            lname: val.lname,
        };
      if(getId()){
      console.log("apiVal",apiVal)
        // apiToastSuccess("User updated successfully")
        editUser(editapiVal)
      }else{
        apiToastSuccess("User added successfully")
        addUser(apiVal)
      }
      history.push('/')
    };

    

    
  return (
    <React.Fragment>
      <Container className="mt-4">
       <Formik
            enableReinitialize
            initialValues={initialValues()}
            validationSchema={ValidationSchema}
            onSubmit={onHandleSubmit}
            >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                submitCount,
                setFieldValue,
            }) => {
                return (
                    <>                    
                        <form onSubmit={handleSubmit} id="quickForm">
                            <Typography variant="h5" gutterBottom>
                               {
                                   (getId())?"Edit User":"Add User"
                               }
                            </Typography>
                            <Grid container spacing={3}>
                             {/* First Name Input */}
                                <Grid item xs={12} sm={6}>
                                <Field
                                    name="fname"
                                    label="First name"
                                    autoComplete="given-name"
                                    className="form-control"
                                    placeholder="First Name..."
                                />
                                {errors.fname && touched.fname ? (
                                                <div className="error">{errors.fname}</div>
                                            ) : null}
                                </Grid>

                            {/* Second Name Input */}
                                <Grid item xs={12} sm={6}>
                                <Field
                                    name="lname"
                                    label="Last name"
                                    autoComplete="family-name"
                                    className="form-control"
                                    placeholder="Last Name..."
                                />
                                {errors.lname && touched.lname ? (
                                                <div className="error">{errors.lname}</div>
                                            ) : null}
                                </Grid>

                            {/* Submit Button */}
                                <Grid item xs={12} sm={12}>
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        type="submit"
                                        className="mx-2"
                                    >
                                    {
                                        (getId())?"Edit":"Add"
                                    }
                                    </Button>
                                    <Button
                                        variant="contained"
                                        color="danger"
                                        type="submit"
                                    >
                                    <Link to="/" className="textAlign">Cancel</Link>
                                    </Button>
                                </Grid>
                            </Grid>
                   </form>
            
                </>
            );
        }}
        </Formik>
      </Container>
    </React.Fragment>
  );
}