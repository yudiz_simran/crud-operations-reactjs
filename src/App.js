import logo from './logo.svg';
import './App.css';

/*****************************Bootstrap**************************************/
import 'bootstrap/dist/css/bootstrap.min.css';

/********************************Css file*****************************/
import "react-toastify/dist/ReactToastify.css";

/********************************React-Packages*****************************/
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { ToastContainer, Flip } from "react-toastify"

/********************************Components*****************************/
import Home from './Components/Home/Home';
import AddUser from './Components/Add-User/addUser';
import editUser from './Components/Edit-User/editUser';
import Header from './Components/Header/header'
import { GlobalProvider } from './Context/GlobalState';

function App() {
  return (
    <>
       <div className="App">
         <GlobalProvider>
          <Router>
            <Header />
              <Switch>
                <Route exact path="/edit/:id" component={AddUser}></Route>
                <Route exact path="/add" component={AddUser}></Route>
                <Route exact path="/" component={Home}></Route>
              </Switch>
            </Router>
         </GlobalProvider>
      {/* Toastify notifications */}
         <ToastContainer
              position="top-right"
              transition={Flip}
              autoClose={3000}
              hideProgressBar={true}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              // pauseOnFocusLoss
              draggable
              pauseOnHover
         />
       </div>
    </>
  );
}

export default App;
