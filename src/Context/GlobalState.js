import React, { createContext, useReducer } from 'react';
import AppReducer from './AppReducer'


/*************************Initial State************************/
const initialState = {
    users:[
        {
            id:"1",
            fname:"Simran",
            lname:"Kaur"
        },
        {
            id:"2",
            fname:"Yogeeta",
            lname:"Maheshwari"
        },
        {
            id:"3",
            fname:"Simi",
            lname:"Kaur"
        },
        {
            id:"4",
            fname:"Axita",
            lname:"Panchal"
        },
        {
            id:"5",
            fname:"Nayela",
            lname:"Desai"
        },
        {
            id:"6",
            fname:"Anjali",
            lname:"Kumari"
        },
        {
            id:"7",
            fname:"Disha",
            lname:"Davda"
        },
        {
            id:"8",
            fname:"Pooja",
            lname:"Singh"
        },
    ]
}

/*************************Create Context************************/
export const GlobalContext = createContext(initialState);

/*************************Provider Component************************/
export const GlobalProvider = ({children}) => {
    const [state,dispatch] = useReducer(AppReducer,initialState)

/*************************Actions-REmove User************************/
    const removeUser = (id) => {
       
        dispatch({
            type:'REMOVE_USER',
            payload:id
        })
    }
/*************************Action- Add User************************/
    const addUser = (user) => {
        dispatch({
            type:'ADD_USER',
            payload:user
        })
    }

/*************************Action- Edit User************************/
const editUser = (user) => {
    console.log("----user",user);
    dispatch({
        type:'EDIT_USER',
        payload:user
    })
}
    return(
        <GlobalContext.Provider  value={{
            users:state.users,
            addUser,
            editUser,
            removeUser
        }}>
            {children}
        </GlobalContext.Provider>
    )
}
